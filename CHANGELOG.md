
# Changelog for Gateway Email Reader Hook

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.0.0] - 2021-02-23

Ported to git

Enhanced read of vhost through new method to get the gateways

## [v1.0.0] - 2014-03-16

First release 
