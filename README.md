# gCube System -Gateway Email Reader Hook

This component is a Liferay 6.2.6 CE Hook which starts at portal startup and triggers the email parser for posts and replies
	
## Structure of the project

* The source code is present in the src folder. 

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

* No Documentation is provided

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/${project.artifactId}/releases).

## Authors

* **Massimiliano Assante** - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## Maintainers

* **Massimiliano Assante** - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:


- the Sixth Framework Programme for Research and Technological Development
    - [DILIGENT](https://cordis.europa.eu/project/id/004260) (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - [D4Science](https://cordis.europa.eu/project/id/212488) (grant no. 212488);
    - [D4Science-II](https://cordis.europa.eu/project/id/239019) (grant no.239019);
    - [ENVRI](https://cordis.europa.eu/project/id/283465) (grant no. 283465);
    - [iMarine](https://cordis.europa.eu/project/id/283644) (grant no. 283644);
    - [EUBrazilOpenBio](https://cordis.europa.eu/project/id/288754) (grant no. 288754).
- the H2020 research and innovation programme 
    - [SoBigData](https://cordis.europa.eu/project/id/654024) (grant no. 654024);
    - [PARTHENOS](https://cordis.europa.eu/project/id/654119) (grant no. 654119);
    - [EGI-Engage](https://cordis.europa.eu/project/id/654142) (grant no. 654142);
    - [ENVRI PLUS](https://cordis.europa.eu/project/id/654182) (grant no. 654182);
    - [BlueBRIDGE](https://cordis.europa.eu/project/id/675680) (grant no. 675680);
    - [PerformFISH](https://cordis.europa.eu/project/id/727610) (grant no. 727610);
    - [AGINFRA PLUS](https://cordis.europa.eu/project/id/731001) (grant no. 731001);
    - [DESIRA](https://cordis.europa.eu/project/id/818194) (grant no. 818194);
    - [ARIADNEplus](https://cordis.europa.eu/project/id/823914) (grant no. 823914);
    - [RISIS 2](https://cordis.europa.eu/project/id/824091) (grant no. 824091);
    - [EOSC-Pillar](https://cordis.europa.eu/project/id/857650) (grant no. 857650);
    - [Blue Cloud](https://cordis.europa.eu/project/id/862409) (grant no. 862409);
    - [SoBigData-PlusPlus](https://cordis.europa.eu/project/id/871042) (grant no. 871042);
    