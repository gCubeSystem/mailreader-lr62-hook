package org.gcube.portal;


import com.liferay.portal.model.Group;

public class Gateway {
	private Group site;

	public Gateway(Group site) {
		super();
		this.site = site;
	
	}
	public Group getSite() {
		return site;
	}
	public void setSite(Group site) {
		this.site = site;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Gateway [site=");
		builder.append(site);
		builder.append("]");
		return builder.toString();
	}

}
