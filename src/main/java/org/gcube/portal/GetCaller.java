package org.gcube.portal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class GetCaller extends Thread {
	private final int WAITING_MILLISECONDS = 900000; 

	private final static String USER_AGENT = "Mozilla/5.0";

	protected static final String SERVLET_CONTEXT_MAIL = "/social-mail-servlet/start-email-reader";

	private List<String> urlsToContact;

	public GetCaller(List<String> urlsToContact) {
		super();
		this.urlsToContact = urlsToContact;
	}



	@Override
	public void run() {
		try {
			System.out.println("********** READING MAILS THREAD Waiting ... starting in " + WAITING_MILLISECONDS/60000 + " minutes");
			Thread.sleep(WAITING_MILLISECONDS);			
			for (String url : urlsToContact) {
				String response = sendHTTPsGet(url+SERVLET_CONTEXT_MAIL);
				System.out.println("**********\n\n"+ "Called URL=" + url + " correctly, response:" + response);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private String sendHTTPsGet(String url) throws Exception {		

		URL obj = new URL(null, url, new sun.net.www.protocol.https.Handler());

		SSLUtilities.trustAllHostnames();
		SSLUtilities.trustAllHttpsCertificates();

		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		con.setRequestProperty("User-Agent", USER_AGENT);
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		return response.toString();
	}
}


