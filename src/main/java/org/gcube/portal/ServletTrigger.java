package org.gcube.portal;

import java.util.ArrayList;
import java.util.List;

import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.util.ManagementUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SimpleAction;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.LayoutSet;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.VirtualHostLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

public class ServletTrigger extends SimpleAction {
	private static final Logger _log = LoggerFactory.getLogger(ServletTrigger.class);

	private final String PROTOCOL = "https://";

	/* (non-Java-doc)
	 * @see com.liferay.portal.kernel.events.SimpleAction#SimpleAction()
	 */
	public ServletTrigger() {
		super();
	}

	/* (non-Java-doc)
	 * @see com.liferay.portal.kernel.events.SimpleAction#run(String[] arg0)
	 */
	public void run(String[] arg0) throws ActionException {		
		try {
			List<String> urlsToContact = getVirtualHostURLs();
			GetCaller gc = new GetCaller(urlsToContact);
			gc.start();

		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	/**
	 * I am containg the same servlet for each vHost
	 * @return
	 */
	private List<String> getVirtualHostURLs() {
		List<String> toReturn = new ArrayList<>();
		_log.info("MAIL_PARSER STARTING ... getGateways() ");
		GroupManager gm = new LiferayGroupManager();
		List<Gateway> gateways = getGateways(gm);
		try {
			for (Gateway gateway : gateways) {
				LayoutSet layoutSet = null;
				Group childGroup = gateway.getSite();
				if (childGroup.hasPublicLayouts()) 
					layoutSet = childGroup.getPublicLayoutSet();
				else 
					layoutSet = childGroup.getPrivateLayoutSet();
				final long companyId = PortalUtil.getDefaultCompanyId();
				long layoutSetId = layoutSet.getLayoutSetId();
				String virtualHost = VirtualHostLocalServiceUtil.getVirtualHost(companyId, layoutSetId).getHostname();
				String hostToContact = PROTOCOL+virtualHost;
				toReturn.add(hostToContact);
				_log.info("\n*** MAILReader - Added host to contact: " + hostToContact);
			}
		}	catch (Exception e) {
			e.printStackTrace();
		}
		return toReturn;
	}

	public List<Gateway> getGateways(GroupManager groupsManager) {
		List<Gateway> toReturn = new ArrayList<>();
		try{
			List<Group> candidateGateways = GroupLocalServiceUtil.getGroups(ManagementUtils.getCompany().getCompanyId(), 0, true);		
			// real gateways have no children as well
			for (Group group : candidateGateways) {
				List<Group> children = group.getChildren(true);
				if(children == null || children.isEmpty())
					if(! (group.getFriendlyURL().equals("/guest") || group.getFriendlyURL().equals("/global") )) {// skipping these sites
						toReturn.add(new Gateway(group));
					}
			}
		} catch(Exception e){
			_log.error("Failed to retrieve the list of gateways", e);
			return null;
		}
		return toReturn;
	}	

}